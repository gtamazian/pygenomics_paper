#!/usr/bin/env python3

"""Reduce regions of genomic repeats annotated by RepeatMasker."""

import gzip
import itertools
import sys
from typing import Iterator

from pygenomics import bed
from pygenomics.interval import GenomicBase, GenomicInterval
from pygenomics_ext import repeatmasker


def read_repeatmasker_repeats(path: str) -> Iterator[GenomicInterval]:
    """Iterate ranges of RepeatMasker repeats."""
    with gzip.open(path, "rt", encoding="utf-8") as out_file:
        for line in map(str.rstrip, itertools.islice(out_file, 3, None)):
            record = repeatmasker.Record.of_string(line)
            assert record is not None
            yield (
                record.query.sequence,
                record.query.match.start,
                record.query.match.end,
            )


def reduce_intervals(repeats: Iterator[GenomicInterval]) -> Iterator[GenomicInterval]:
    """Reduce the repeat interval."""
    base = GenomicBase(repeats)
    return GenomicBase.iterate_reduced(base)


def print_bed(reduced_repeats: Iterator[GenomicInterval]) -> None:
    """Print the reduced repeat intervals in the BED format."""
    for seq, start, end in reduced_repeats:
        print(str(bed.Record(bed.Bed3(seq, bed.Range(start - 1, end)))))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 demo.py repeatmasker.out.gz", file=sys.stderr)
        sys.exit(1)
    print_bed(reduce_intervals(read_repeatmasker_repeats(sys.argv[1])))
