"""Sample the specified number of records from a RepeatMasker .out file."""

import gzip
import itertools
import random
from typing import List


def read_header(path: str) -> List[str]:
    """Read a RepeatMasker .out file header."""
    with gzip.open(path, "rt", encoding="utf-8") as out_file:
        return [str.rstrip(k) for _, k in zip(range(3), out_file)]


def sample_lines(path: str, num_lines: int) -> List[str]:
    """Sample the specified number of repeat lines."""
    with gzip.open(path, "rt", encoding="utf-8") as out_file:
        return random.sample(
            [str.rstrip(k) for k in itertools.islice(out_file, 3, None)], k=num_lines
        )


def main(in_path: str, sample_size: int, out_path: int) -> None:
    """Main function of the script."""
    with gzip.open(out_path, "wt", encoding="utf-8") as out_file:
        for line in itertools.chain(
            read_header(in_path), sample_lines(in_path, sample_size)
        ):
            print(line, file=out_file)


if __name__ == "__main__":
    main(snakemake.input[0], snakemake.params["sample_size"], snakemake.output[0])
