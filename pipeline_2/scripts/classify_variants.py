"""Classify SNPs by their regions and transitios-transversions."""

import gzip
import itertools
import sys
from collections import Counter, defaultdict
from typing import DefaultDict, Dict, Iterable, Iterator, List, NamedTuple, Set, Tuple
from typing import Optional, TypeVar

from pygenomics.cli.interval.common import gff3_to_range, vcf_to_range
from pygenomics.cli.interval.interval_complement import get_edges
from pygenomics.common import assoc_single
from pygenomics.interval import GenomicBase, GenomicInterval
from pygenomics.gff import gff3
from pygenomics.vcf.data import genotype
from pygenomics.vcf.data.record import GenotypeFields
from pygenomics.vcf.data.record import Record as VcfRecord
from pygenomics.vcf.reader import Reader as VcfReader


class IncorrectGff3Line(Exception):
    """Indicate an incorrect GFF3 line."""

    path: str
    line: str

    def __init__(self, path: str, line: str):
        super().__init__(path, line)
        self.path = path
        self.line = line


def iterate_gff3_records(path: str) -> Iterator[gff3.Record]:
    """Iterate parsed records of a GFF3 file."""
    with gzip.open(path, "rt", encoding="utf-8") as gff_file:
        for line in map(
            str.rstrip, filter(lambda x: not str.startswith(x, "#"), gff_file)
        ):
            record = gff3.Record.of_string(line)
            if record is None:
                raise IncorrectGff3Line(path, line)
            yield record


ChromosomeInfo = List[Tuple[str, Tuple[str, int]]]


class GenomeInfo(NamedTuple):
    """Chromosome information and gene records."""

    chrom_info: ChromosomeInfo
    protein_coding_genes: List[gff3.Record]
    lncrna_genes: List[gff3.Record]


def get_genome_info(path: str) -> GenomeInfo:
    """Get genome information from the specified GFF3 file."""
    chrom_names: Set[str] = set()
    chrom_info: ChromosomeInfo = []
    protein_coding_genes: List[gff3.Record] = []
    lncrna_genes: List[gff3.Record] = []
    for record in iterate_gff3_records(path):
        if (
            record.pos.feature_type == "region"
            and assoc_single(record.attributes.pairs, "genome") == "chromosome"
        ):
            chromosome_name = assoc_single(record.attributes.pairs, "chromosome")
            assert chromosome_name is not None
            set.add(chrom_names, record.pos.seqid)
            list.append(
                chrom_info,
                (chromosome_name, (record.pos.seqid, record.pos.end)),
            )
        elif record.pos.feature_type == "gene" and record.pos.seqid in chrom_names:
            gene_type = assoc_single(record.attributes.pairs, "gene_biotype")
            if gene_type == "protein_coding":
                list.append(protein_coding_genes, record)
            elif gene_type == "lncRNA":
                list.append(lncrna_genes, record)

    return GenomeInfo(chrom_info, protein_coding_genes, lncrna_genes)


def change_chrom_name(interval: GenomicInterval, chrom: str) -> GenomicInterval:
    """Change the chromosome name for an interval."""
    return (chrom, interval[1], interval[2])


def get_intergenic_base(genome: GenomeInfo, chrom: str) -> GenomicBase:
    """Get genomic intervals for regions outside annotated genes."""
    chrom_info = assoc_single(genome.chrom_info, chrom)
    assert chrom_info is not None
    chrom_accn, chrom_len = chrom_info
    gene_base = GenomicBase(
        change_chrom_name(gff3_to_range(k), chrom)
        for k in itertools.chain(genome.protein_coding_genes, genome.lncrna_genes)
        if k.pos.seqid == chrom_accn
    )

    return GenomicBase(
        itertools.chain(
            GenomicBase.iterate_complement(gene_base),
            get_edges(GenomicBase.seq_ranges(gene_base), [(chrom, chrom_len)]),
        )
    )


def get_lncrna_base(genome: GenomeInfo, chrom: str) -> GenomicBase:
    """Get genomic intervals for lncRNA genes."""
    chrom_info = assoc_single(genome.chrom_info, chrom)
    assert chrom_info is not None
    chrom_accn, _ = chrom_info
    return GenomicBase(
        change_chrom_name(gff3_to_range(k), chrom)
        for k in genome.lncrna_genes
        if k.pos.seqid == chrom_accn
    )


def get_reduced_base(intervals: Iterable[GenomicInterval]) -> GenomicBase:
    """Build the genomic base of reduced intervals."""
    return GenomicBase(GenomicBase.iterate_reduced(GenomicBase(intervals)))


def get_exon_intron_bases(
    path: str, genome: GenomeInfo, chrom: str
) -> Tuple[GenomicBase, GenomicBase]:
    """Get regions of exons for specified gene records."""

    chrom_info = assoc_single(genome.chrom_info, chrom)
    assert chrom_info is not None
    chrom_accn, _ = chrom_info

    genes: Dict[str, gff3.Record] = {}
    for k in genome.protein_coding_genes:
        if k.pos.seqid == chrom_accn:
            current_id = assoc_single(k.attributes.pairs, "ID")
            assert current_id is not None and current_id not in genes
            genes[current_id] = k

    mrna_ids: Dict[str, str] = {}
    exons: DefaultDict[str, List[gff3.Record]] = defaultdict(list)
    for k in iterate_gff3_records(path):
        if k.pos.feature_type == "mRNA":
            current_id = assoc_single(k.attributes.pairs, "ID")
            current_parent = assoc_single(k.attributes.pairs, "Parent")
            assert current_id is not None and current_parent is not None
            if current_parent in genes:
                mrna_ids[current_id] = current_parent
        elif k.pos.feature_type == "exon":
            current_parent = assoc_single(k.attributes.pairs, "Parent")
            assert current_parent is not None
            if current_parent in mrna_ids:
                exons[mrna_ids[current_parent]] += [k]

    exon_base = get_reduced_base(
        change_chrom_name(gff3_to_range(k), chrom)
        for gene in dict.values(exons)
        for k in gene
    )

    intron_base = get_reduced_base(
        itertools.chain.from_iterable(
            GenomicBase.subtract(
                GenomicBase(
                    change_chrom_name(gff3_to_range(k), chrom) for k in exons[gene_id]
                ),
                change_chrom_name(gff3_to_range(genes[gene_id]), chrom),
            )
            for gene_id in dict.keys(exons)
        )
    )

    return (exon_base, intron_base)


class GenomicRegions(NamedTuple):
    """Genomic bases for various parts of the genome."""

    exons: GenomicBase
    introns: GenomicBase
    intergenic: GenomicBase
    lncrna: GenomicBase


def get_genomic_bases(path: str, chrom: str) -> GenomicRegions:
    """Get bases for exons, introns, lncRNAs, miRNAs, and intergenic regions."""
    genome_info = get_genome_info(path)
    intergenic_base = get_intergenic_base(genome_info, chrom)
    lncrna_base = get_lncrna_base(genome_info, chrom)
    exon_base, intron_base = get_exon_intron_bases(path, genome_info, chrom)
    return GenomicRegions(
        exon_base,
        intron_base,
        intergenic_base,
        lncrna_base,
    )


def is_snp(variant: VcfRecord) -> bool:
    """Is the variant an SNP?"""
    assert len(variant.alt) == 1
    return (
        variant.alt[0].content is not None
        and len(variant.ref.bases) == len(variant.alt[0].content) == 1
    )


def get_snp_type(variant: VcfRecord) -> str:
    """Get the SNP type: a transition or a transversion."""
    ref = variant.ref.bases
    alt = variant.alt[0].content
    assert len(variant.alt) == 1 and alt is not None
    alleles = tuple(sorted([ref, alt]))
    return "transition" if alleles in {("A", "G"), ("C", "T")} else "transversion"


def classify_variant(regions: GenomicRegions, variant: VcfRecord) -> str:
    """Classify a variant by its genomic region."""
    interval = vcf_to_range(variant)
    if GenomicBase.find(regions.exons, interval):
        return "exon"
    if GenomicBase.find(regions.introns, interval):
        return "intron"
    if GenomicBase.find(regions.intergenic, interval):
        return "intergenic"
    if GenomicBase.find(regions.lncrna, interval):
        return "lncrna"
    return "NA"


_T = TypeVar("_T")


def assure_not_none(value: Optional[_T]) -> _T:
    """Assure that the specified value is not None."""
    assert value is not None
    return value


def get_heterozygotes(samples: List[str], variant: VcfRecord) -> List[str]:
    """Get samples that have heterozygote genotypes."""
    fields = GenotypeFields.of_string(variant.genotype_line)
    assert fields is not None

    return [
        j
        for j, k in zip(
            samples,
            (genotype.GenotypeRecord.of_string(k.parts[0][0]) for k in fields.samples),
        )
        if genotype.GenotypeRecord.is_heterozygous(assure_not_none(k))
    ]


def analyze_variants(vcf_path: str, gff_path: str, chrom: str, out_path: str) -> None:
    """Analyze SNPs from a VCF file according to their locations."""
    regions = get_genomic_bases(gff_path, chrom)
    with gzip.open(vcf_path, "rt", encoding="utf-8") as vcf_file:
        reader = VcfReader(vcf_file)
        counts = Counter(
            (k, get_snp_type(variant), classify_variant(regions, variant))
            for variant in reader.data
            if is_snp(variant)
            for k in get_heterozygotes(reader.samples, variant)
        )

    with open(out_path, "wt", encoding="utf-8") as out_file:
        for (sample, snp_type, snp_region), snp_count in dict.items(counts):
            print(
                str.format(
                    "{:s}\t{:s}\t{:s}\t{:d}", sample, snp_type, snp_region, snp_count
                ),
                file=out_file
            )


if __name__ == "__main__":
    analyze_variants(
        str(snakemake.input["variants"]),
        str(snakemake.input["genes"]),
        snakemake.params["chrom"],
        str(snakemake.output[0])
    )
