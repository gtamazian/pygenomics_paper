#!/usr/bin/env python3

"""Simple CLI for benchmarking PyRanges routines."""

import argparse
import os.path

import pyranges


def is_bed(path: str) -> bool:
    """Check if the file name has the BED extension."""
    _, extension = os.path.splitext(path)
    return extension == ".bed"


def read_file(path: str):
    """Read a GFF3 or BED file."""
    return pyranges.read_bed(path) if is_bed(path) else pyranges.read_gff3(path)


def parse_args() -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(description="Simple CLI for PyRanges.")
    parser.add_argument(
        "operation",
        choices=["intersect", "overlap", "subtract"],
        help="interval operation",
    )
    parser.add_argument("target", help="target file name")
    parser.add_argument("query", help="query file name")
    return parser.parse_args()


def main() -> None:
    """Main function of the script."""
    args = parse_args()
    query = read_file(args.query)
    target = read_file(args.target)
    if args.operation == "intersect":
        print(query.intersect(target).to_bed())
    elif args.operation == "overlap":
        print(query.overlap(target).to_bed())
    else:
        assert args.operation == "subtract"
        print(query.subtract(target).to_bed())


if __name__ == "__main__":
    main()
