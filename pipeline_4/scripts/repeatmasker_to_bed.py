#!/usr/bin/env python3

"""Print repeat regions from a RepeatMasker .out file in the BED format."""

import gzip
import itertools
import sys

from pygenomics import bed
from pygenomics_ext import repeatmasker


def process_repeatmasker_repeats(path: str) -> None:
    """Print repeat ranges in the BED format."""
    with gzip.open(path, "rt", encoding="utf-8") as out_file:
        for line in map(str.rstrip, itertools.islice(out_file, 3, None)):
            record = repeatmasker.Record.of_string(line)
            assert record is not None
            print(
                bed.Record(
                    bed.Bed3(
                        record.query.sequence,
                        bed.Range(record.query.match.start - 1, record.query.match.end),
                    )
                )
            )


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 repeatmasker_to_bed.py repeats.out.gz", file=sys.stderr)
        sys.exit(1)
    process_repeatmasker_repeats(sys.argv[1])
