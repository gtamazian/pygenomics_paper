#!/usr/bin/env python3

"""Get GFF3 records for genes and exons of protein-coding genes."""

import argparse
from typing import Dict, List, Optional, Set

from pygenomics.cli.interval.common import iterate_gff3_records
from pygenomics.common import assoc_single
from pygenomics.gff import base, gff3


def get_chromosome_names(path: str) -> Dict[str, str]:
    """Get the dictionary of chromosome accessions and names."""
    names: Dict[str, str] = {}
    for k in iterate_gff3_records(path):
        if (
            k.pos.feature_type == "region"
            and assoc_single(k.attributes.pairs, "genome") == "chromosome"
        ):
            chromosome_name = assoc_single(k.attributes.pairs, "chromosome")
            assert chromosome_name is not None and k.pos.seqid not in names
            names[k.pos.seqid] = chromosome_name
    return names


def get_coding_genes(path: str) -> List[gff3.Record]:
    """Get GFF3 records of protein-coding genes."""
    coding_genes: List[gff3.Record] = []
    for k in iterate_gff3_records(path):
        if k.pos.feature_type == "gene":
            gene_type = assoc_single(k.attributes.pairs, "gene_biotype")
            if gene_type == "protein_coding":
                list.append(coding_genes, k)
    return coding_genes


def get_record_id(record: gff3.Record) -> str:
    """Get the ID attribute of a GFF3 record."""
    id_value = assoc_single(record.attributes.pairs, "ID")
    assert id_value is not None
    return id_value


def get_record_parent(record: gff3.Record) -> str:
    """Get the Parent attribute of a GFF3 record."""
    parent = assoc_single(record.attributes.pairs, "Parent")
    assert parent is not None
    return parent


def get_exons(path: str, genes: List[gff3.Record]) -> List[gff3.Record]:
    """Get GFF3 records of protein-coding gene exons."""
    gene_ids: Set[str] = {get_record_id(k) for k in genes}
    mrna_ids: Set[str] = set()
    exons: List[gff3.Record] = []
    for k in iterate_gff3_records(path):
        if k.pos.feature_type == "mRNA" and get_record_parent(k) in gene_ids:
            set.add(mrna_ids, get_record_id(k))
        elif k.pos.feature_type == "exon" and get_record_parent(k) in mrna_ids:
            list.append(exons, k)
    return exons


def update_seq(record: gff3.Record, new_seq: str) -> gff3.Record:
    """Update sequence of a GFF3 record."""
    return gff3.Record(
        base.Record(
            new_seq,
            record.pos.source,
            record.pos.feature_type,
            record.pos.record_range,
            record.pos.score,
            record.pos.strand,
            record.pos.phase,
        ),
        record.attributes,
    )


def write_gff3_file(
    path: str, records: List[gff3.Record], seq_names: Dict[str, str]
) -> None:
    """Write GFF3 records to the specified file."""
    with open(path, "w", encoding="utf-8") as gff3_file:
        for k in records:
            if k.pos.seqid in seq_names:
                print(update_seq(k, "chr" + seq_names[k.pos.seqid]), file=gff3_file)


def parse_args(args: Optional[List[str]] = None):
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(
        description="Extract gene and exon GFF3 records for protein-coding genes."
    )
    parser.add_argument("gff3_file", help="input GFF3 file")
    parser.add_argument("out_genes", help="output GFF3 file of gene records.")
    parser.add_argument("out_exons", help="output GFF3 file of exon records.")
    return parser.parse_args(args)


def main(args: argparse.Namespace) -> None:
    """Main function of the script."""
    names = get_chromosome_names(args.gff3_file)
    genes = get_coding_genes(args.gff3_file)
    exons = get_exons(args.gff3_file, genes)
    write_gff3_file(args.out_genes, genes, names)
    write_gff3_file(args.out_exons, exons, names)


if __name__ == "__main__":
    main(parse_args())
