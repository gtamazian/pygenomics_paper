#!/usr/bin/env python3

"""Simple CLI for benchmarking pybedtools routines."""

import argparse

import pybedtools


def parse_args() -> argparse.Namespace:
    """Parse command-line arguments of the script."""
    parser = argparse.ArgumentParser(description="Simple CLI for PyRanges.")
    parser.add_argument(
        "operation",
        choices=["intersect", "overlap", "subtract"],
        help="interval operation",
    )
    parser.add_argument("target", help="target file name")
    parser.add_argument("query", help="query file name")
    return parser.parse_args()


def main() -> None:
    """Main function of the script."""
    args = parse_args()
    query_tool = pybedtools.BedTool(args.query)
    target_tool = pybedtools.BedTool(args.target)
    if args.operation == "intersect":
        print(query_tool.intersect(target_tool))
    elif args.operation == "overlap":
        print(query_tool.intersect(target_tool, wa=True))
    else:
        assert args.operation == "subtract"
        print(query_tool.subtract(target_tool))


if __name__ == "__main__":
    main()
