# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- Add error bars to performance plots
- Increase the number of benchmark runs from 5 to 30

## [0.1.1] - 2023-01-05

### Added

- Add a pipeline for benchmarking reading VCF files
- Add a pipeline for benchmarking reading BAM files
- Add a pipeline for benchmarking interval operations

### Changed

- Produce plots in the TikZ format

## [0.1.0] - 2022-06-24

### Added

- Initial release of the project

[unreleased]: https://gitlab.com/gtamazian/pygenomics-paper/-/compare/0.1.1...main
[0.1.1]: https://gitlab.com/gtamazian/pygenomics-paper/-/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/gtamazian/pygenomics-paper/-/tags/0.1.0
