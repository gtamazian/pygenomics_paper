# pygenomics-paper

This repository contains pipelines that accompany the following
submitted manuscript:

Gaik Tamazian, Nikolay Cherkasov, Alexander Kanapin and Anastasia
Samsonova. Pygenomics: manipulating genomic intervals and data files
in Python. 2022.

## Pipelines

The following table lists the pipelines with the related data formats.

| Pipeline   | Formats                | Analysis                                                                                                                                                                                                                                        |
|------------|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Pipeline 1 | BAM, RepeatMasker .out | Genomic repeat coverage by aligned reads                                                                                                                                                                                                        |
| Pipeline 2 | GFF3, VCF              | Distribution of transition to transversion ratios                                                                                                                                                                                               |
| Pipeline 3 | RepeatMasker .out      | Performance comparison of CPython and PyPy                                                                                                                                                                                                      |
| Pipeline 4 | BED, GFF3              | Performance of interval operations for [_pygenomics_](https://gitlab.com/gtamazian/pygenomics), [_pybedtools_](https://github.com/daler/pybedtools) and [_PyRanges_](https://github.com/biocore-ntnu/pyranges)                    |
| Pipeline 5 | BAM                    | Performance of reading and processing read alignments for [_pygenomics_](https://gitlab.com/gtamazian/pygenomics) and [_pysam_](https://github.com/pysam-developers/pysam)                                                        |
| Pipeline 6 | VCF                    | Performance of reading and processing genomic variant records for [_pygenomics_](https://gitlab.com/gtamazian/pygenomics), [_pysam_](https://github.com/pysam-developers/pysam) and [_cyvcf2_](https://github.com/brentp/cyvcf2/) |

The pipelines are implemented in
[_Snakemake_](https://snakemake.github.io/). They require Python
packages [_pygenomics_](https://gitlab.com/gtamazian/pygenomics) and
[_pygenomics-ext_](https://gitlab.com/gtamazian/pygenomics-ext)
installed and also [R](https://www.r-project.org/) with the
[_tidyverse_](https://www.tidyverse.org/) package collection.
