#!/usr/bin/env python3

"""Get biallelic SNPs in the HGVS format using pysam routines."""

import sys

import pysam


def is_bialleic_snp(variant: pysam.VariantRecord) -> bool:
    """Check if the specified variant represents a biallelic SNP."""
    return (
        variant.ref is not None
        and len(variant.ref) == 1
        and variant.alts is not None
        and len(variant.alts) == 1
        and variant.alts[0] is not None
        and len(variant.alts[0]) == 1
    )


def to_hgvs(variant: pysam.VariantRecord) -> str:
    """Get the HGVS representation for a biallelic SNP."""
    assert variant.alts is not None
    return str.format(
        "{:s}:g.{:d}{:s}>{:s}", variant.chrom, variant.pos, variant.ref, variant.alts[0]
    )


def main(path: str) -> None:
    """Main function of the script."""
    for variant in pysam.VariantFile(path):
        if is_bialleic_snp(variant):
            print(to_hgvs(variant))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(
            "Usage: python3 get_biallelic_snps_pysam.py variants.vcf.gz",
            file=sys.stderr,
        )
        sys.exit(1)
    main(sys.argv[1])
