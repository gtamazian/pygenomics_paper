#!/usr/bin/env python3

"""Get biallelic SNPs in the HGVS format using cyvcf2 rotuines."""

import sys

import cyvcf2


def is_bialleic_snp(variant: cyvcf2.Variant) -> bool:
    """Check if the specified variant represent a biallelic SNP."""
    return len(variant.REF) == len(variant.ALT) == 1 and len(variant.ALT[0]) == 1


def to_hgvs(variant: cyvcf2.Variant) -> str:
    """Get the HGVS representation for a biallelic SNP."""
    return str.format(
        "{:s}:g.{:d}{:s}>{:s}", variant.CHROM, variant.POS, variant.REF, variant.ALT[0]
    )


def main(path: str) -> None:
    """Main function of the script."""
    for variant in cyvcf2.VCF(path):
        if is_bialleic_snp(variant):
            print(to_hgvs(variant))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(
            "Usage: python3 get_biallelic_snps_cyvcf2.py variants.vcf.gz",
            file=sys.stderr,
        )
        sys.exit(1)
    main(sys.argv[1])
