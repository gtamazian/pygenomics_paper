library(dplyr)
library(ggplot2)
library(purrr)
library(readr)
library(scales)
library(stringr)
library(tikzDevice)

read_benchmark <- function(path) {
    parts <- stringr::str_split(path, "/", simplify = TRUE)
    sample_size <- parts %>%
        .[3] %>%
        stringr::str_split(stringr::fixed("."), simplify = TRUE) %>%
        .[1]
    readr::read_tsv(path) %>%
        dplyr::summarize(time = s, memory = max_rss) %>%
        dplyr::mutate(
            tool = factor(parts[2], levels = c("pygenomics", "pysam", "cyvcf2")),
            sample_size = sample_size,
        )
}

norm_ci <- function(x, level) {
    x_mean <- mean(x)
    n <- length(x)
    x_sigma <- sd(x)
    alpha <- qnorm(1 - level / 2)
    delta <- alpha * x_sigma / sqrt(n)
    tibble(Mean = x_mean, Low = x_mean - delta, High = x_mean + delta)
}

summarize_time <- function(df) {
    df %>%
        dplyr::select(-memory) %>%
        dplyr::group_by(tool, sample_size) %>%
        dplyr::group_map(function(x, y) {
            tibble::tibble(y, norm_ci(x$time, 0.05))
        }) %>%
        dplyr::bind_rows()
}

summarize_memory <- function(df) {
    df %>%
        dplyr::select(-time) %>%
        dplyr::group_by(tool, sample_size) %>%
        dplyr::group_map(function(x, y) {
            tibble::tibble(y, norm_ci(x$memory, 0.05))
        }) %>%
        dplyr::bind_rows()
}

load_data <- function(path) {
    purrr::map(
        dir(path, full.names = TRUE, recursive = TRUE),
        read_benchmark
    ) %>%
        dplyr::bind_rows() %>%
        dplyr::mutate(sample_size = as.numeric(sample_size))
}

produce_plots <- function(path, out_prefix) {
    x <- load_data(path)

    tikz(stringr::str_c(out_prefix, "time_variants.tex", sep = "/"),
        width = 3.3, height = 4.2
    )
    plot <- ggplot(
        summarize_time(x),
        aes(x = sample_size, y = Mean, color = tool)
    ) +
        geom_point() +
        geom_errorbar(aes(ymin = Low, ymax = High)) +
        geom_line() +
        scale_x_continuous(
            label = label_comma(suffix = "K", scale = 1e-3),
            n.breaks = 9, minor_breaks = NULL
        ) +
        scale_y_continuous(n.breaks = 5) +
        theme_bw() +
        xlab("Number of variant records") +
        ylab("Running time in seconds") +
        theme(
            legend.position = "bottom",
            axis.title.x = element_text(vjust = -2),
            axis.text.x = element_text(angle = 45, hjust = 1),
            legend.title = element_blank(),
            axis.text.y = element_text(angle = 90, hjust = .5),
        )
    print(plot)
    dev.off()

    tikz(stringr::str_c(out_prefix, "memory_variants.tex", sep = "/"),
        width = 3.3, height = 4.2
    )
    plot <- ggplot(
        summarize_memory(x),
        aes(x = sample_size, y = Mean, color = tool)
    ) +
        geom_point() +
        geom_errorbar(aes(ymin = Low, ymax = High)) +
        geom_line() +
        scale_x_continuous(
            label = label_comma(suffix = "K", scale = 1e-3),
            n.breaks = 9, minor_breaks = NULL
        ) +
        scale_y_continuous(n.breaks = 5) +
        theme_bw() +
        xlab("Number of variant records") +
        ylab("Maximum resident set size in megabytes") +
        theme(
            legend.position = "bottom",
            axis.title.x = element_text(vjust = -2),
            axis.text.x = element_text(angle = 45, hjust = 1),
            legend.title = element_blank(),
            axis.text.y = element_text(angle = 90, hjust = .5),
        )
    print(plot)
    dev.off()
}

produce_plots(snakemake@params[["benchmark"]], snakemake@params[["out_prefix"]])
