#!/usr/bin/env python3

"""Get biallelic SNPs in the HGVS format using pygenomics routines."""

import gzip
import sys

from pygenomics.vcf.data.record import Record
from pygenomics.vcf.reader import Reader


def is_bialleic_snp(variant: Record) -> bool:
    """Check if the specified variant represents a biallelic SNP."""
    return (
        len(variant.ref.bases) == len(variant.alt) == 1
        and variant.alt[0].content is not None
        and len(variant.alt[0].content) == 1
    )


def to_hgvs(variant: Record) -> str:
    """Get the HGVS representation for a biallelic SNP."""
    return str.format(
        "{}:g.{}{}>{}", variant.chrom, variant.pos, variant.ref, variant.alt[0]
    )


def main(path: str) -> None:
    """Main function of the script."""
    with gzip.open(path, "rt", encoding="utf-8") as vcf_file:
        vcf_reader = Reader(vcf_file)
        for variant in vcf_reader.data:
            if is_bialleic_snp(variant):
                print(to_hgvs(variant))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(
            "Usage: python3 get_biallelic_snps_pygenomics.py variants.vcf.gz",
            file=sys.stderr,
        )
        sys.exit(1)
    main(sys.argv[1])
