#!/usr/bin/env python3

"""Count properly paired reads in a BAM file using pysam."""

import sys

import pysam


def count_proper_pairs(path: str) -> int:
    """Count properly paired reads in the specified BAM file."""
    return sum(k.is_proper_pair for k in pysam.AlignmentFile(path))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 pp_count_pysam.py alignments.bam", file=sys.stderr)
        sys.exit(1)
    print(count_proper_pairs(sys.argv[1]))
