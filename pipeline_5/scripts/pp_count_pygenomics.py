#!/usr/bin/env python3

"""Count segments of properly paired reads in a BAM file using pygenomics."""

import gzip
import sys

from pygenomics.bam.reader import Reader as BamReader
from pygenomics.sam.alignment import Flag


def count_proper_pairs(path: str) -> int:
    """Count segments of properly paired reads."""
    with gzip.open(path, "rb") as bam_file:
        reader = BamReader(bam_file)
        return sum(
            k.prefix.flag & Flag.PROPERLY_ALIGNED.value for k in reader.alignments
        )


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 pp_count_pygenomics.py alignments.bam", file=sys.stderr)
        sys.exit(1)
    print(count_proper_pairs(sys.argv[1]))
