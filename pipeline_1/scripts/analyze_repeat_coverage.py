"""Estimate coverage of repeats by aligned reads."""

import gzip
import itertools
import statistics
import sys
from typing import Dict, Iterable, Iterator, List, Tuple

from pygenomics.bam.alignment import decode_cigar_op
from pygenomics.bam.reader import Reader as BamReader
from pygenomics.interval import GenomicBase, GenomicInterval
from pygenomics.sam.alignment import Flag as SamFlag
from pygenomics_ext import repeatmasker


class IncorrectRepeatMaskerLine(Exception):
    """Indicate a RepeatMasker line that could not be parsed."""

    line: str

    def __init__(self, line: str):
        super().__init__(line)
        self.line = line


def load_repeats(out_path: str, chrom: str) -> List[repeatmasker.Record]:
    """Load records of RepeatMasker repeats for the specified chromosome."""
    with gzip.open(out_path, "rt", encoding="utf-8") as out_file:
        result: List[repeatmasker.Record] = []
        for line in map(str.rstrip, itertools.islice(out_file, 4, None)):
            parsed_record = repeatmasker.Record.of_string(line)
            if parsed_record is None:
                raise IncorrectRepeatMaskerLine(line)
            if parsed_record.query.sequence == chrom:
                list.append(result, parsed_record)
        return result


_PREFIX = "chr"
_PREFIX_LEN = len(_PREFIX)


def process_chrom_name(name: str) -> str:
    """Remove the prefix from a chromosome name."""
    return name[_PREFIX_LEN:] if str.startswith(name, _PREFIX) else name


RepeatInfo = Dict[GenomicInterval, repeatmasker.Record]


def build_repeat_base(
    records: List[repeatmasker.Record],
) -> Tuple[GenomicBase, RepeatInfo]:
    """Get the tree of repeats and the dictionary mapping the intervals to the records."""
    return (
        GenomicBase(
            (
                process_chrom_name(k.query.sequence),
                k.query.match.start - 1,
                k.query.match.end - 1,
            )
            for k in records
        ),
        {
            (
                process_chrom_name(k.query.sequence),
                k.query.match.start - 1,
                k.query.match.end - 1,
            ): k
            for k in records
        },
    )


CoverageMap = Dict[GenomicInterval, List[int]]


def create_coverage_map(intervals: Iterable[GenomicInterval]) -> CoverageMap:
    """Create the dictionary of repeat intervals and their coverage maps."""
    return {k: [0] * (k[2] - k[1] + 1) for k in intervals}


Cigar = List[Tuple[str, int]]


def unfold_cigar_rle(cigar: Cigar) -> str:
    """Unfold the CIGAR operation record."""
    return str.join("", (code * times for code, times in cigar))


AlignedRead = Tuple[GenomicInterval, str]


def iterate_read_intervals(reader: BamReader) -> Iterator[AlignedRead]:
    """Iterate read alignment ranges and CIGAR operations."""
    for k in reader.alignments:
        if k.prefix.flag & SamFlag.UNMAPPED.value:
            continue
        seq = reader.header.ref_sequences[k.prefix.ref_id][0]
        if k.prefix.flag & SamFlag.REVERSED.value:
            start = k.prefix.pos - k.prefix.l_seq + 1
            end = k.prefix.pos
        else:
            start = k.prefix.pos
            end = k.prefix.pos + k.prefix.l_seq - 1

        cigar = unfold_cigar_rle([decode_cigar_op(j) for j in k.cigar])
        yield ((seq, start, end), cigar[: k.prefix.l_seq])


def cigar_to_coverage(cigar_line: str) -> List[int]:
    """Map a CIGAR line to the coverage mask."""
    return [int(k in {"M", "=", "X"}) for k in cigar_line]


def add_coverage(
    repeats: GenomicBase, coverage: CoverageMap, aligned_read: AlignedRead
) -> None:
    """Add the coverage by the read to the map for specified repeats.

    The function produces a side effect by modifying its `coverage` argument.

    """
    read, cigar = aligned_read
    _, start, end = read
    overlaps = GenomicBase.find_all(repeats, read)
    if overlaps:
        read_coverage_mask = cigar_to_coverage(cigar)
        for match in overlaps:
            _, match_start, match_end = match
            if start < match_start and match_end < end:
                # the match is within the read
                adjusted_mask = read_coverage_mask[
                    (match_start - start) : -(end - match_end)
                ]
            elif start >= match_start and match_end < end:
                adjusted_mask = [0] * (start - match_start) + read_coverage_mask[
                    : -(end - match_end)
                ]
            elif start < match_start and match_end >= end:
                adjusted_mask = read_coverage_mask[(match_start - start) :] + [0] * (
                    match_end - end
                )
            else:
                # start >= match_start and match_end >= end
                adjusted_mask = (
                    [0] * (start - match_start)
                    + read_coverage_mask
                    + [0] * (match_end - end)
                )
            coverage[match] = [j + k for j, k in zip(adjusted_mask, coverage[match])]


def summarize_coverage(
    rmout_path: str, chrom: str, bam_path: str
) -> Tuple[RepeatInfo, CoverageMap]:
    """Summarize coverage of repeats by aligned reads."""
    repeat_base, repeat_info = build_repeat_base(load_repeats(rmout_path, chrom))
    repeat_coverage_map = create_coverage_map(dict.keys(repeat_info))
    with gzip.open(bam_path, "rb") as bam_file:
        for read in iterate_read_intervals(BamReader(bam_file)):
            add_coverage(repeat_base, repeat_coverage_map, read)
    return (repeat_info, repeat_coverage_map)


def print_coverage(out_data: Tuple[RepeatInfo, CoverageMap], out_path: str) -> None:
    """Print the mean coverage for every repeat."""
    info, coverage_map = out_data
    with open(out_path, "wt", encoding="utf-8") as out_file:
        for interval, extra in dict.items(info):
            print(
                str.format(
                    "{:s}\t{:d}\t{:d}\t{:s}\t{:s}\t{:s}\t{:g}",
                    interval[0],
                    interval[1] - 1,
                    interval[2],
                    extra.repeat.rep_class,
                    extra.repeat.rep_family
                    if extra.repeat.rep_family is not None
                    else "NA",
                    extra.repeat.rep_name,
                    statistics.mean(coverage_map[interval]),
                ),
                file=out_file,
            )


if __name__ == "__main__":
    print_coverage(
        summarize_coverage(
            str(snakemake.input["repeats"]),
            snakemake.params["chrom"],
            str(snakemake.input["bam"]),
        ),
        str(snakemake.output[0]),
    )
